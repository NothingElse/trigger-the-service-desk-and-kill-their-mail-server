﻿using System;
using System.Net.NetworkInformation;

namespace TriggerServiceDesk
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PROGRAM STARTED");
            while (true)
            {
                Console.WriteLine("pinging ww.google.de...");
                bool checkInternet = true;
                while (checkInternet)
                {
                    try
                    {
                        checkInternet = new Ping().Send("www.google.de",5000).Status == IPStatus.Success;
                        System.Threading.Thread.Sleep(5000);
                    }
                    catch (Exception e)
                    {
                        checkInternet = false;
                    }
                }
                if (!checkInternet)
                {
                    DateTime starttime = DateTime.Now;
                    Console.WriteLine("***********PING ERROR***************");
                    Console.WriteLine("Do you want to send a Mail? (J/N)");
                    string answer = Console.ReadLine();
                    if (answer.ToUpper() == "J")
                    {
                        Console.WriteLine("Bitte Vor-und Nachnamen angeben:");
                        string name = Console.ReadLine();
                        Console.WriteLine("Bitte Notebookmodell angeben:");
                        string model = Console.ReadLine();
                        Console.WriteLine("Bitte Raum angeben:");
                        string room = Console.ReadLine();
                        Console.WriteLine("Bitte Problem beschreiben:");
                        string problem = Console.ReadLine();

                        Console.WriteLine("Waiting for reconnection to send the Mail and trigger the servicedesk...");
                        while (!checkInternet)
                        {
                            try
                            {
                                checkInternet = new Ping().Send("www.google.de", 5000).Status == IPStatus.Success;
                                System.Threading.Thread.Sleep(5000);
                            }
                            catch (Exception e)
                            {
                                checkInternet = false;
                            }
                        }
                        DateTime endTime = DateTime.Now;
                        string mailText = mailBody(name, model, problem, starttime, endTime, room);
                        Console.WriteLine("Sending Mail...");
                        Sendmail("servicedesk@hs-osnabrueck.de", mailText);
                    }
                    Console.WriteLine("************************************");
                }
            }
        }
        public static void Sendmail(string EmailTo, string MailBody, string Subject = "WLAN Probleme")
        {
            try
            {
                Microsoft.Office.Interop.Outlook.NameSpace lo_NSpace;
                Microsoft.Office.Interop.Outlook.MAPIFolder lo_Folder;
                Microsoft.Office.Interop.Outlook.Application lo_OutApp;
                Microsoft.Office.Interop.Outlook.MailItem lo_Item;

                lo_OutApp = new Microsoft.Office.Interop.Outlook.Application();

                lo_NSpace = lo_OutApp.GetNamespace("MAPI");

                lo_Folder = lo_NSpace.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderSentMail);

                lo_Item = (Microsoft.Office.Interop.Outlook.MailItem)lo_Folder.Items.Add(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);

                lo_Item.To = EmailTo;

                //Wenn man cc oder bcc verschicken will
                //lo_Item.CC = EmailCC;
                //lo_Item.BCC = EmailBCC;

                //wenn man möchte kann man noch Flaggen und Fälligkeit definieren
                //lo_Item.FlagStatus = Microsoft.Office.Interop.Outlook.OlFlagStatus.olFlagMarked;
                //lo_Item.FlagIcon = Microsoft.Office.Interop.Outlook.OlFlagIcon.olRedFlagIcon;
                //lo_Item.FlagDueBy = DateTime.Now.AddDays(7);

                lo_Item.Subject = Subject;
                lo_Item.HTMLBody = MailBody;
                //NachrichtenFormat
                lo_Item.BodyFormat = Microsoft.Office.Interop.Outlook.OlBodyFormat.olFormatHTML;

                //Anzeigen modal
                lo_Item.Display(false);

                //Senden der Mail
                lo_Item.Send();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        static public string mailBody(string name, string model, string problem, DateTime startTime, DateTime endTime, string room)
        {
            string HostName = System.Net.Dns.GetHostName();
            System.Net.IPHostEntry hostInfo = System.Net.Dns.GetHostByName(HostName);
            string ipAdresse = hostInfo.AddressList[0].ToString();
            string text = "<html>"
                                 + "<body>"
                                    + "<p>Guten Tag,</p>"
                                    + "<p>Ich konnte Netzwerkprobleme feststellen.</p>"
                                    + $"<p>* Zeitpunkt/Zeitraum: {startTime} - {endTime} </p>"
                                    + $"<p>Gebäude/Raumnummer: {room}</p>  "
                                    + "<p>* Benutztes Gerät</p>  "
                                    + "<p>- Notebook</p>  "
                                    + $"<p>- {model}</p>  "
                                    + "<p>- Windows 10</p>  "
                                    + $"<p>* {ipAdresse}</p>  "
                                    + $"<p>* Art des Fehlers: {problem} </p>  "
                                    + $"<p>Mit freundlichen Grüßen,<br>{name}</br></p>"
                                 + "</body>"
                                + "</html>";
            return text;
        }


    }
}
